#!/bin/bash

# Calc
# http://devbraindom.blogspot.com/2013/03/eliminate-led-screen-flicker-with-intel.html
# Пишу, что там ошибка: менять нужно только старшие 4 цифры
#
# Периодически горизонтальная рябь на эране, проблема не ясна.
# Нет информации об отсутствии проблем при длительной эксплуатации
# при частотах выше 600 Гц

# Set 240 Hz PWM
#/usr/bin/intel_reg write 0xC8254 0xfe50fe5

# Set 600 Hz PWM
#/usr/bin/intel_reg write 0xC8254 0x065c0000

# Set 1200 Hz PWM
/usr/bin/intel_reg write 0xC8254 0x032e0000

# Set 2100 Hz PWM
#/usr/bin/intel_reg write 0xC8254 0x1d10000

# Set 6000 Hz PWM
#/usr/bin/intel_reg write 0xC8254 0xa300a3
